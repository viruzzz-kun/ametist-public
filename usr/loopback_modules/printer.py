# !/usr/bin/env python
# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re

__author__ = 'viruzzz-kun'
__created__ = '31.05.2014'

printedition = re.compile(
    '(VV|VO|WW|WO)RS((01|06|12|17|18)RUVG|(0[1689]|16|18)RUMH|(0[15678]|10|11)RUYL|0[246]RUST|0[1245]RUGP)'
)

def process(Message):
    if printedition.match(Message.get('ahl', '').upper()):
        i = Message.clone()
        i['ahl'] = 'PRINTMEOUT'
        i['destinations'] = {'unimas'}
        return [i]

