# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from autobahn.twisted.websocket import WebSocketServerFactory, WebSocketServerProtocol
import json
import logging

__author__ = 'viruzzz-kun'


class BroadcastServerProtocol(WebSocketServerProtocol):
    def __init__(self):
        self.subscribed_message_types = {}

    def onOpen(self):
        self.factory.register(self)

    def connectionLost(self, reason):
        WebSocketServerProtocol.connectionLost(self, reason)
        self.factory.unregister(self)

    def onMessage(self, payload, isBinary):
        self.messageReceived(json.loads(payload))

    def messageReceived(self, message):
        if not message:
            return
        subscribe = message.get('subscribe', None)
        if subscribe:
            for name, config in subscribe.items():
                self.subscribed_message_types[name] = config

    def send_msg(self, data, variant):
        self.sendMessage(json.dumps({'variant': variant, 'data': data}))

    def is_subscribed_to(self, variant):
        return variant in self.subscribed_message_types


class BroadcastServerFactory(WebSocketServerFactory):
    """
    Simple broadcast server broadcasting any message it receives to all
    currently connected clients.
    """

    def __init__(self):
        from server.thecore import core
        port = core.get_config('flask-app')['port']
        WebSocketServerFactory.__init__(self, externalPort=port)
        self.protocol = BroadcastServerProtocol
        self.setProtocolOptions(allowHixie76=True)
        self.clients = []
        self.__stats_lc = None

    def register(self, client):
        """
        @type client: BroadcastServerProtocol
        """
        if not client in self.clients:
            logging.debug("registered client %s", client.peer)
            self.clients.append(client)

    def unregister(self, client):
        """
        @type client: BroadcastServerProtocol
        """
        if client in self.clients:
            logging.debug("unregistered client %s", client.peer)
            self.clients.remove(client)

    def broadcast(self, obj, variant):
        preparedMsg = self.prepareMessage(json.dumps({'variant': variant, 'data': obj}))
        for c in self.clients:
            if c.is_subscribed_to(variant):
                c.sendPreparedMessage(preparedMsg)

    def prepare_msg(self, data, variant):
        return self.prepareMessage(json.dumps({'variant': variant, 'data': data}))

    def send_to_many(self, preparedMsg, clients):
        for c in clients:
            c.sendPreparedMessage(preparedMsg)

    def send_to_one(self, preparedMsg, client):
        client.sendPreparedMessage(preparedMsg)

    def filter_clients(self, variant):
        return [client for client in self.clients if client.is_subscribed_to(variant)]
