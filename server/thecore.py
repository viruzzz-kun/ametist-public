# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import time
from twisted.internet import reactor
from twisted.internet.defer import Deferred
from twisted.internet.task import defer, LoopingCall
from twisted.internet.error import ReactorNotRunning
import os
import yaml

from server.channel import Phase, ChannelWrapper
from server.ioj.wrapper import IOJWrapper
from libametist.utils import format_duration

__date__ = "21.04.2013"
__author__ = "viruzzz-kun"


class ConfigLoader(yaml.Loader):
    def __init__(self, stream):
        self._root = os.path.split(stream.name)[0]
        super(ConfigLoader, self).__init__(stream)

    def include(self, node):
        filename = os.path.join(self._root, self.construct_scalar(node))
        with open(filename, 'r') as f:
            return yaml.load(f, ConfigLoader)

ConfigLoader.add_constructor('!include', ConfigLoader.include)


class Core(object):
    """The Core itself"""

    def __init__(self):
        self.config = self.load_config()
        self.ioj = IOJWrapper()
        self.channels = {}
        self.expected_phase = Phase.stopped
        self.phase = Phase.stopped
        self.ws_factory = None
        self.time_time = time.time()
        self.time_clock = time.clock()
        self.__defer_init = None
        self.__periodical = LoopingCall(self.__send_stats)

    @staticmethod
    def load_config():
        configPath = os.environ.get('AMETIST_CONFIG_PATH', './etc/ametist/ametist.yaml')
        with open(configPath) as configFile:
            return yaml.load(configFile, ConfigLoader)

    def get_config(self, subsystem, name=None):
        if subsystem in ('ioj', 'flask-app', 'routing', 'queue-pool'):
            return self.config[subsystem]
        elif subsystem == 'channels':
            return self.config[subsystem][name]
        return self.config.get(subsystem, {})

    # System functions

    @defer.inlineCallbacks
    def dispatch_message(self, message, name, magic=None):
        """Dispatch received message.

        @param message: received message
        @param name: originating channel name"""
        # logging.debug('Received from [%s] %s (%s)', name, message.get('ahl', ''), message.tags)
        message['origin'] = name
        try:
            from chain import ForwardChainProcessor
            processor = ForwardChainProcessor(message)
            processor.process()
        except:
            # Whatever happens, keep on going! We still need to put it in IOJ and get its UID
            pass
        finally:
            uid = yield self.ioj.put(message)
            message.uid = uid
            if magic:
                reactor.callLater(0, self.__ack_magic, name, magic)
            self.ws_factory.broadcast({
                'uid': message.uid,
                'origin': message['origin'],
                'destinations': list(message['destinations']),
                'tags': list(message.tags),
                'time': time.time(),
                'ahl': message.get('ahl'),
            }, 'msgbrief')
            reactor.callLater(0, self.proceed_routes, message)

    def inject_message(self, message):
        reactor.callLater(0, self.proceed_routes, message)

    def proceed_routes(self, message):
        for dest in message['destinations']:
            if dest in self.channels:
                self.channels[dest].queue.push(message)

    def __ack_magic(self, name, magic):
        self.channels[name].sendObject({
            'command': 'msg_ack',
            'magic': magic
        })

    @defer.inlineCallbacks
    def initialize(self):
        """Initialize core. Create channel wrappers and start XMLRPC Server"""
        if self.__defer_init:
            defer.returnValue(None)
        self.__defer_init = Deferred()
        logging.info("Initializing Core...")

        config = self.get_config('flask-app')
        # Set up Flask.app
        from twisted.web.server import Site
        from twisted.web.wsgi import WSGIResource
        from www.app import app
        from websockets import BroadcastServerFactory
        from autobahn.twisted.resource import WebSocketResource, WSGIRootResource, HTTPChannelHixie76Aware

        ws_factory = BroadcastServerFactory()
        self.ws_factory = ws_factory

        wsgi_resource = WSGIResource(reactor, reactor.getThreadPool(), app)
        ws_resource = WebSocketResource(ws_factory)
        root_resource = WSGIRootResource(
            wsgi_resource, {
                config.get('ws.path', 'ws'): ws_resource
            }
        )

        site = Site(root_resource)
        site.protocol = HTTPChannelHixie76Aware

        reactor.listenTCP(config.get('port', 7666), site, interface=config.get('interface', '127.0.0.1'))

        yield self.ioj.init()

        for name in self.config['channels']:
            logging.info("Creating channel %s..." % name)
            self.channels[name] = ChannelWrapper(name)

        self.__periodical.start(1)

        defer_init = self.__defer_init
        self.__defer_init = None
        defer_init.callback(None)

    def __send_stats(self):
        self.ws_factory.broadcast(self.get_stat(), 'stats')

    # Exported functions

    def start_channel(self, name):
        """Start a channel

        @param name: channel unique name"""
        if not self.phase in (Phase.running, Phase.starting):
            return
        logging.info('Starting channel %s', name)
        self.channels[name].start()

    def stop_channel(self, name):
        """Stop a channel

        @param name: channel unique name"""
        if not self.phase in (Phase.running, Phase.stopping):
            return
        logging.info('Stopping channel %s', name)
        self.channels[name].stop()

    def restart_channel(self, name):
        if not self.phase in (Phase.running, Phase.starting):
            return
        logging.info('Restarting channel %s', name)
        self.channels[name].restart()

    def purge_channel(self, name):
        """Purge channel' queue

        @param name: channel unique name"""
        logging.info('Purging queue for channel %s', name)
        self.channels[name].queue.purge()

    def start(self):
        """Start core and channels"""
        if self.__defer_init:
            self.__defer_init.addCallback(self.__start_int)
        else:
            self.__start_int()

    def __start_int(self, _=None):
        self.expected_phase = Phase.running
        if self.phase != Phase.stopped:
            return
        logging.info('Starting Core...')
        self.phase = Phase.starting
        for name in self.config['channels']:
            self.start_channel(name)
        self.phase = Phase.running

    def stop(self, _=None):
        """Stop core and channels"""
        if self.__defer_init:
            self.__defer_init.addCallback(self.__stop_int)
        else:
            self.__stop_int()

    def __stop_int(self):
        self.expected_phase = Phase.stopped
        if self.phase != Phase.running:
            return
        logging.info('Stopping Core...')
        self.phase = Phase.stopping
        for name in self.config['channels']:
            self.stop_channel(name)
        self.phase = Phase.stopped

    def unload(self):
        """Stop core and channels and unload it"""
        self.stop()
        try:
            reactor.callLater(1, reactor.stop)
        except ReactorNotRunning:
            pass

    def get_stat(self):
        return {
            'phase': self.phase,
            'channels': dict((name, chan.getStats()) for name, chan in self.channels.iteritems()),
            'time_time': format_duration(time.time() - self.time_time),
            'time_clock': (time.clock() - self.time_clock),
        }

core = Core()
