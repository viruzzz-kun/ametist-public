#!/usr/bin/env python
# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import sys
logging.basicConfig(
    format='%(asctime)s: %(process)5d [%(levelname)s] %(filename)s:%(lineno)d %(message)s',
    stream=sys.stderr,
    level=logging.DEBUG
)
from server.ioj.ioj import IOJ
from libametist import StreamProtocol

from twisted.internet import reactor
from twisted.internet.protocol import ServerFactory
from twisted.internet.stdio import StandardIO
from twisted.internet.error import ReactorNotRunning

# noinspection PyUnresolvedReferences
import libametist.formats

__date__ = "10.04.2014"
__author__ = "viruzzz-kun"

class ModuleException(BaseException):
    pass


class IOJProcess(object):
    """Base class for channel modules. Provides basic functionality for communication with the Core"""

    class Controller(StreamProtocol):
        def __init__(self, app):
            StreamProtocol.__init__(self)
            self.app = app
            self.primary = False

        def command_put(self, obj):
            message = obj['message']
            magic = obj['magic']
            uid = self.app.ioj.put(message)
            self.sendObject({
                'command': 'put_ack',
                'uid': uid,
                'magic': magic,
            })

        def command_get(self, obj):
            uid = obj['uid']
            try:
                message = self.app.ioj[uid]
            except KeyError, e:
                self.sendObject({
                    'command': 'get_resp',
                    'exception': e,
                    'uid': uid,
                    'magic': obj['magic'],
                })
            else:
                self.sendObject({
                    'command': 'get_resp',
                    'message': message,
                    'uid': uid,
                    'magic': obj['magic'],
                })

        def command_slice(self, obj):
            magic = obj['magic']
            config = obj['config']
            logging.debug('Started Async injection')
            try:
                for message in self.app.ioj.get_messages_between(config):
                    self.sendObject({
                        'command': 'slice_resp_async',
                        'message': message,
                        'uid': message.uid,
                        'magic': magic
                    })
            except Exception, e:
                self.sendObject({
                    'command': 'slice_resp_async',
                    'finish': True,
                    'magic': magic,
                    'exception': e,
                })
            else:
                self.sendObject({
                    'command': 'slice_resp_async',
                    'finish': True,
                    'magic': magic
                })
            logging.debug('Finish command emitted')

        def command_get_min_timestamp(self, obj):
            self.sendObject({
                'command': 'min_timestamp',
                'value': self.app.ioj.get_min_timestamp(),
                'magic': obj['magic'],
            })

        def command_config(self, obj):
            if self.primary:
                self.app.configure(obj['config'])


    class ControllerFactory(ServerFactory):
        def __init__(self, app):
            self.app = app

        def buildProtocol(self, addr):
            return IOJProcess.Controller(self.app)

    def __init__(self):
        self.config = None
        self.ioj = None
        self._controller = self.Controller(self)
        self._controller.primary = True
        self._factory = self.ControllerFactory(self)

    def configure(self, config):
        """[override] Configure the Channel Module

        @param config: dict object with the configuration"""
        self.config = config
        logging.info('IOJ got configuration.')
        self.ioj = IOJ(config['path'])
        reactor.listenTCP(
            int(config.get('port', 5666)),
            self._factory,
            interface=config.get('host', '127.0.0.1')
        )
        self._controller.sendObject({
            'command': 'started'
        })

    def run(self):
        StandardIO(self._controller)
        logging.info('Child process starting...')
        reactor.run()

    @classmethod
    def main(cls):
        module = cls()
        module.run()

    def router_messageReceived(self, message):
        """[override, callback] Dispatch message received from Core

        @param message: api.Message"""

    def stop(self):
        """Stop the current process"""
        try:
            reactor.callLater(2, reactor.stop)
        except ReactorNotRunning:
            pass

IOJProcess.main()