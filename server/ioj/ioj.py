# -*- encoding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import calendar
import logging
import io
import os
import re
import struct
import time
from twisted.internet import reactor

from libametist import serialize
from libametist.utils import ensure_dir_exists, safe_traverse


__author__ = 'viruzzz-kun'
__date__ = "10.04.2014"


class TimeHelper(object):
    __slots__ = ['timestamp', 'year', 'month', 'day', 'hour']
    def __init__(self):
        self.timestamp = None
        self.year = None
        self.month = None
        self.day = None
        self.hour = None

    @classmethod
    def from_timestamp(cls, timestamp):
        c = cls()
        c.timestamp = timestamp
        tt = time.gmtime(timestamp)
        c.year = tt.tm_year
        c.month = tt.tm_mon
        c.day = tt.tm_mday
        c.hour = tt.tm_hour
        return c

    @classmethod
    def from_folder_file(cls, folder, filename):
        c = cls()
        c.year = int(folder[0:4])
        c.month = int(folder[4:6])
        c.day = int(filename[0:2])
        c.hour = int(filename[2:4])
        c.timestamp = calendar.timegm(time.struct_time((c.year, c.month, c.day, c.hour, 0, 0, 0, 0, 0)))
        return c

    @classmethod
    def from_key(cls, key):
        c = cls()
        c.year = int(key[0:4])
        c.month = int(key[4:6])
        c.day = int(key[6:8])
        c.hour = int(key[8:10])
        c.timestamp = calendar.timegm(time.struct_time((c.year, c.month, c.day, c.hour, 0, 0, 0, 0, 0)))
        return c


    @classmethod
    def from_four(cls, year, month, day, hour):
        c = cls()
        c.year = year
        c.month = month
        c.day = day
        c.hour = hour
        return c

    @property
    def folder(self):
        return u'%04d%02d' % (self.year, self.month)

    @property
    def filename(self):
        return u'%02d%02d.ioj' % (self.day, self.hour)

    def full_path(self, path):
        return os.path.join(path, self.folder, self.filename)

    def full_dir(self, path):
        return os.path.join(path, self.folder)

    @property
    def short_name(self):
        return os.path.join(self.folder, self.filename)

    def timestamp_applicable(self, timestamp):
        tt = time.gmtime(timestamp)
        return self.year, self.month, self.day, self.hour == tt[0:4]

    @property
    def key(self):
        return '%04d%02d%02d%02d' % (self.year, self.month, self.day, self.hour)


class IOJHandle(object):
    header = struct.Struct('!LLQ')
    handle_close_timeout = 120

    def __init__(self, path, time_helper):
        self.__close_later = reactor.callLater(self.handle_close_timeout, self.close)
        self.time_helper = time_helper
        self.path = path
        self.handle = None
        self.__cache = {}

    def open(self):
        if self.__close_later.active():
            self.__close_later.reset(self.handle_close_timeout)
        else:
            self.__close_later = reactor.callLater(60, self.close)
        if not self.handle or self.handle and self.handle.closed:
            ensure_dir_exists(self.time_helper.full_dir(self.path))
            self.handle = open(self.time_helper.full_path(self.path), 'a+b')
            self.__preload()

    def close(self):
        if self.__close_later.active():
            self.__close_later.cancel()
        if self.handle and not self.handle.closed:
            logging.info(u'IOJHandle <%s> closed', self.time_helper.short_name)
            self.handle.close()
            self.__cache = {}

    @classmethod
    def exists(cls, path, time_helper):
        return os.path.exists(time_helper.full_path(path))

    def uid_applicable(self, uid):
        timestamp = (uid & 0xFFFFFFFFFFFFF000) >> 12
        return self.time_helper.timestamp_applicable(timestamp)

    def __preload(self):
        if not self.handle or self.handle.closed:
            self.open()
            return
        t = time.time()
        # logging.info(u'IOJHandle "%s" preloading...', self.time_helper.short_name)
        handle = self.handle
        handle.seek(0)
        self.__cache = {}
        while 1:
            tell = handle.tell()
            chunk1 = handle.read(self.header.size)
            if not chunk1:
                break
            offset, size, uid = self.header.unpack(chunk1)
            if not offset == tell:
                logging.critical(
                    'IOJHandle <%s> error: recorded offset not equal actual offset',
                    self.time_helper.short_name
                )
                raise Exception
            handle.seek(size, io.SEEK_CUR)
            self.__cache[uid] = (offset, size)
        logging.info(
            u'IOJHandle <%s> preloading finished in %.3f ms',
            self.time_helper.short_name,
            (time.time() - t) * 1000
        )

    def __read(self, offset, size):
        self.open()
        handle = self.handle
        handle.seek(offset + self.header.size)
        data = handle.read(size)
        if len(data) != size:
            raise Exception
        return serialize.load(data)

    def __getitem__(self, uid):
        self.open()
        return self.__read(*self.__cache[uid])

    def __setitem__(self, uid, data):
        self.open()
        handle = self.handle
        chunk = serialize.dump(data)
        handle.seek(0, io.SEEK_END)
        offset = handle.tell()
        size = len(chunk)
        handle.write(self.header.pack(offset, size, uid))
        handle.write(chunk)
        handle.flush()
        self.__cache[uid] = (offset, size)

    def __iter__(self):
        self.open()
        sorted_cache = sorted(self.__cache.iteritems(), key=lambda (k, (o, s)): o)
        for uid, (offset, size) in sorted_cache:
            yield uid, self.__read(offset, size)


class IOJ(object):
    def __init__(self, path):
        self.path = path
        try:
            os.makedirs(self.path)
        except OSError, e:
            if e.errno == 17:
                pass
        self.handles = {}

        self.__last_counter = 0
        self.__last_timestamp = 0
        self.reload()

    def reload(self):
        for handle in self.handles.itervalues():
            handle.close()
        self.handles = {}
        for folder in sorted(os.listdir(self.path)):
            for filename in sorted(os.listdir(os.path.join(self.path, folder))):
                if not filename.endswith('.ioj'):
                    continue
                self.__create_ioj_handle(TimeHelper.from_folder_file(folder, filename))

    def __getitem__(self, uid):
        timestamp = (uid & 0xFFFFFFFFFFFFF000) >> 12
        return self.__get_handle_for_timestamp(timestamp, create=False)[uid]

    def __setitem__(self, uid, data):
        timestamp = (uid & 0xFFFFFFFFFFFFF000) >> 12
        self.__get_handle_for_timestamp(timestamp)[uid] = data

    def __get_handle_for_timestamp(self, timestamp, create=True):
        time_helper = TimeHelper.from_timestamp(timestamp)
        key = time_helper.key
        if key in self.handles:
            return self.handles[key]
        else:
            if create:
                return self.__create_ioj_handle(time_helper)
            raise KeyError

    def __create_ioj_handle(self, time_helper):
        ioj_handle = IOJHandle(self.path, time_helper)
        self.handles[time_helper.key] = ioj_handle
        return ioj_handle

    def get_messages_between(self, config):
        logging.debug('%s', config)
        cond = []
        lcls = dict(locals())
        origin = safe_traverse(config, 'conditions', 'origin')
        if origin is not None:
            cond.append("obj['origin'] in %s" % origin)

        ahl = safe_traverse(config, 'conditions', 'ahl')
        if ahl:
            lcls['re_ahl'] = re.compile(ahl)
            cond.append("re_ahl.match(obj['ahl'])")

        destinations = safe_traverse(config, 'conditions', 'destinations')
        if destinations:
            lcls['plus_dest'] = destinations[0]
            lcls['minus_dest'] = destinations[1]
            cond.append("any((dest in %s) for dest in obj['destinations'])" % destinations[0])
            cond.append("not any((dest in %s) for dest in obj['destinations'])" % destinations[1])

        tags = safe_traverse(config, 'conditions', 'tags')
        if tags:
            lcls['plus_tag'] = tags[0]
            lcls['minus_tag'] = tags[1]
            cond.append("any((tag in %s) for tag in obj.tags)" % tags[0])
            cond.append("not any((tag in %s) for tag in obj.tags)" % tags[1])

        text_eval = " and ".join(cond)
        logging.debug('condition = "%s"', text_eval)

        cond_eval = compile(text_eval, '<condition>', 'eval') if text_eval else compile('True', '<True>', 'eval')

        to_destinations = safe_traverse(config, 'actions', 'destinations')

        def process_message(message):
            if to_destinations:
                message['destinations'] = set(to_destinations)
            return message

        now = time.time()
        timestamp_begin = safe_traverse(config, 'conditions', 'beg', default=now)
        timestamp_end = safe_traverse(config, 'conditions', 'end', default=now)
        timestamp_begin, timestamp_end = sorted([timestamp_begin, timestamp_end])

        time_helper_begin = TimeHelper.from_timestamp(timestamp_begin)
        time_helper_end = TimeHelper.from_timestamp(timestamp_end)

        key_begin = time_helper_begin.key
        key_end = time_helper_end.key

        items = self.handles.items()
        items.sort(key=lambda (k, v): k)
        for key, ioj_handle in items:
            if key_begin <= key <= key_end:
                for uid, obj in ioj_handle:
                    if timestamp_begin < ((uid & 0xFFFFFFFFFFFFF000) >> 12) < timestamp_end:
                        lcls.update(obj=obj)
                        if eval(cond_eval, globals(), lcls):
                            yield process_message(obj)

    def get_new_uid(self):
        timestamp = int(time.time())
        if timestamp == self.__last_timestamp:
            self.__last_counter += 1
        else:
            self.__last_counter = 0
        self.__last_timestamp = timestamp
        return ((timestamp & 0x000FFFFFFFFFFFFF) << 12) + (self.__last_counter & 0x0FFF)

    def put(self, message):
        uid = self.get_new_uid()
        message.uid = uid
        self[uid] = message
        return uid

    def get_min_timestamp(self):
        return TimeHelper.from_key(min(self.handles.iterkeys())).timestamp if len(self.handles) else 0