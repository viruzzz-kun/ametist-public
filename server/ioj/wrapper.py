# coding=utf-8

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os
from twisted.internet import reactor
from twisted.internet.defer import Deferred
from twisted.internet.protocol import ProcessProtocol

from libametist import StreamProtocol


__author__ = 'viruzzz-kun'
__date__ = "10.04.2014"


class IOJWrapper(ProcessProtocol, StreamProtocol):
    """Core-side Subprocess representation."""
    exe = 'server/ioj/process.py'

    def __init__(self):
        """Constructor"""
        StreamProtocol.__init__(self)
        self.send_map = {}
        self.recv_map = {}
        self.injectables = {}
        self.started = False
        self.death_counter = 0
        self.cannot_start = False
        self.__started = None

    def init(self):
        self.__started = Deferred()
        self._spawn()
        return self.__started

    def stop(self):
        # self.transport.loseConnection()
        self.transport.signalProcess('KILL')

    def outReceived(self, data):
        StreamProtocol.dataReceived(self, data)

    def processEnded(self, reason):
        from server.thecore import core
        self.started = False
        core.stop()
        logging.critical("IOJ Stopped")

    def _spawn(self):
        """Spawn the channel process"""
        exe = os.path.join(os.getcwdu(), self.exe)
        env = dict(os.environ)
        env['PYTHONPATH'] = os.getcwdu()
        reactor.spawnProcess(
            self, executable=exe, args=(exe, ),
            env=env,
            childFDs={0: 'w', 1: 'r', 2: 2}
        )

    def connectionMade(self):
        """[callback] When channel process is started and connected, sent it its configuration"""
        from server.thecore import core
        logging.info("IOJ started")
        self.sendObject({
            'command': 'config',
            'config': core.get_config('ioj'),
        })

    def command_put_ack(self, obj):
        magic = obj['magic']
        deferred = self.send_map.pop(magic)
        deferred.callback(obj['uid'])

    def command_get_resp(self, obj):
        magic = obj['magic']
        deferred = self.recv_map.pop(magic)
        if 'message' in obj:
            deferred.callback(obj['message'])
        else:
            deferred.errback(obj['exception'])

    def command_slice_resp_async(self, obj):
        magic = obj['magic']
        if not magic in self.injectables:
            return
        if 'finish' in obj:
            num, _, finished = self.injectables.pop(magic)
            if callable(finished):
                finished()
            logging.debug('Finish received. %s messages', num)
        else:
            self.injectables[magic][0] += 1
            self.injectables[magic][1](obj['message'])

    def command_min_timestamp(self, obj):
        magic = obj['magic']
        deferred = self.recv_map.pop(magic)
        deferred.callback(obj['value'])

    def command_started(self, _):
        if self.__started:
            self.__started.callback(True)
            self.__started = None

    def put(self, message):
        """Put the message to the IOJ

        @param message: api.Message to send"""
        magic = os.urandom(16)
        obj = {
            'command': 'put',
            'message': message,
            'magic': magic,
        }
        self.sendObject(obj)
        self.send_map[magic] = d = Deferred()
        return d

    def get(self, uid):
        magic = os.urandom(16)
        obj = {
            'command': 'get',
            'uid': uid,
            'magic': magic
        }
        self.sendObject(obj)
        self.recv_map[magic] = d = Deferred()
        return d

    def filter(self, config, callback, callback_finished=None):
        magic = os.urandom(16)
        obj = {
            'command': 'slice',
            'async': True,
            'magic': magic,
            'config': config,
        }
        self.injectables[magic] = [0, callback, callback_finished]
        self.sendObject(obj)
        return magic

    def get_min_timestamp(self):
        magic = os.urandom(16)
        obj = {
            'command': 'get_min_timestamp',
            'magic': magic,
        }
        self.recv_map[magic] = d = Deferred()
        self.sendObject(obj)
        return d