# !/usr/bin/env python
# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import time
from twisted.internet import reactor
from twisted.internet.protocol import ProcessProtocol
from libametist import StreamProtocol
from server.rapid_queue import Queue

__author__ = 'viruzzz-kun'
__created__ = '08.06.2014'


class Phase(object):
    stopped = 0
    starting = 1
    running = 2
    stopping = 3

    codes = {
        stopped: 'Stopped',
        starting: 'Starting',
        running: 'Running',
        stopping: 'Stopping',
    }

    @classmethod
    def str(cls, code):
        return cls.codes.get(code, 'Unknown')


class ChannelWrapper(ProcessProtocol, StreamProtocol):
    """Core-side Channel representation."""

    def __init__(self, name):
        """Constructor

        @param name: channel's unique name"""
        from server.thecore import core

        self.config = core.get_config('channels', name)
        self.config['name'] = name
        self.name = name
        self._stat_sent = 0
        self._stat_recv = 0
        self._extState = None
        self.expected_phase = Phase.stopped
        self.phase = Phase.stopped
        self.queue = Queue(self.name, self.sendMessage)
        self.last_run_timestamp = 0
        self.death_counter = 0
        self.cannot_start = False
        StreamProtocol.__init__(self)


    def start(self):
        self.cannot_start = False
        self.expected_phase = Phase.running
        if self.phase in (Phase.starting, Phase.running, Phase.stopping):
            return
        self.phase = Phase.starting
        self.clearBuffer()
        self.last_run_timestamp = time.time()
        self._spawn()

    def stop(self):
        self.expected_phase = Phase.stopped
        if self.phase in (Phase.stopped, Phase.stopping, Phase.starting):
            return
        self.phase = Phase.stopping
        self.sendObject({'command': 'stop'})

    def restart(self):
        self.expected_phase = Phase.running
        if not self.phase == Phase.running:
            return
        self.phase = Phase.stopping
        self.sendObject({'command': 'stop'})

    def outReceived(self, data):
        StreamProtocol.dataReceived(self, data)

    def processEnded(self, reason):
        logging.info("%s exited with code: %s" % (self.name, reason.value.exitCode))
        self.queue.stop()
        self._extState = None
        self.queue.reset()
        self.phase = Phase.stopped
        if self.expected_phase == Phase.running:
            self._restart_decision()

    def _restart_decision(self):
        now = time.time()
        if (now - self.last_run_timestamp) < 2:
            self.death_counter += 1
        else:
            self.death_counter = 0
        if self.death_counter < 5:
            reactor.callLater(0.5, self.start)
        else:
            self.death_counter = 0
            self.cannot_start = True
            self.expected_phase = Phase.stopped
            logging.critical('%s restarted 5 times running less than 2 seconds. Seems like, it\'s a problem', self.name)

    def _spawn(self):
        """Spawn the channel process"""
        exe = os.path.join(os.getcwdu(), 'plugins', self.config['module'])
        uid = self.config.get('uid', None)
        gid = self.config.get('gid', None)
        env = dict(os.environ)
        env['PYTHONPATH'] = os.getcwdu()
        reactor.spawnProcess(
            self, executable=exe, args=(exe, ),
            env=env,
            uid=uid, gid=gid, childFDs={0: 'w', 1: 'r', 2: 2}
        )

    def connectionMade(self):
        """[callback] When channel process is started and connected, sent it its configuration"""
        self.phase = Phase.running
        if self.expected_phase == 0:
            self.sendObject({'command': 'stop'})
            return
        self.sendObject({
            'command': 'config',
            'config': self.config,
        })
        self.queue.reset()
        self.queue.start()

    def sendMessage(self, message):
        """[callback] Send the message to the channel process. Used by QueueServer.Queue

        @param message: api.Message to send"""
        obj = {
            'command': 'message',
            'message': message,
            'magic': os.urandom(16),
        }
        self.sendObject(obj)

    def getStats(self):
        return {
            'sent': self._stat_sent,
            'received': self._stat_recv,
            'queued': len(self.queue),
            'backed': len(self.queue.backed),
            'phase': self.phase,
            'extState': u'cannot start' if self.cannot_start else self._extState,
        }

    def command_message(self, obj):
        from server.thecore import core

        self._stat_recv += 1
        core.dispatch_message(obj['message'], self.name, obj.get('magic'))

    def command_msg_ack(self, obj):
        self._stat_sent += 1
        self.queue.ack(obj['message_id'])

    def command_msg_nack(self, obj):
        self.queue.nack(obj['message_id'])

    def command_extState(self, obj):
        self._extState = obj['extState']