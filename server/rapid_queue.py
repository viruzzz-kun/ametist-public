# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import logging
import os
import struct
from contextlib import contextmanager
from twisted.internet import reactor, defer
from twisted.internet.task import LoopingCall

from libametist.utils import ensure_dir_exists


__author__ = 'viruzzz-kun'


class Queue(object):
    record = struct.Struct('!Q')
    compaction_threshold = 1000
    backlog = 5

    # API

    def __init__(self, name, sendMessage):
        self.name = name
        self.sendMessage = sendMessage
        from server.thecore import core
        config = core.get_config('queue-pool', name)
        root_path = config['root']
        file_path = os.path.join(root_path, name)
        self.filepath = file_path
        self.objects = {}
        ensure_dir_exists(root_path)
        self.handle = open(file_path, 'a+b')
        self.queue = []
        self.backed = set()
        self.__consistent = False
        self.__preload()
        self.__dumper = None

    def start(self):
        if self.__dumper and self.__dumper.running:
            self.__dumper.stop()
        self.__dumper = LoopingCall(self.__dump)
        self.__dumper.start(10, False)
        self.__shake()

    def stop(self):
        self.__dumper.stop()
        self.__dump()

    def push(self, message):
        uid = message.uid
        if uid in self.queue:
            logging.warn('Duplicate message uid pushed: %s', uid)
            return
        self.objects[uid] = message
        self.handle.write(self.record.pack(uid))
        self.handle.flush()
        self.queue.append(uid)
        self.__shake()

    def ack(self, uid):
        if uid in self.backed:
            self.backed.remove(uid)
            self.queue.remove(uid)
            self.objects.pop(uid, None)
        self.__shake()

    def nack(self, uid):
        if uid in self.backed:
            self.backed.remove(uid)
        self.__shake()

    def purge(self):
        self.backed = set()
        self.queue = []
        self.objects = {}
        self.__dump()
        self.__shake()

    def reset(self):
        self.backed = set()

    def __len__(self):
        return len(self.queue)

    # Private API

    def __shake(self):
        """Shake the queue :)"""
        if not self.__consistent:
            return
        while len(self.backed) < min(self.backlog, len(self.queue)):
            for uid in self.queue:
                if not uid in self.backed:
                    self.backed.add(uid)
                    reactor.callLater(0, self.sendMessage, self.objects[uid])
                    break

    @defer.inlineCallbacks
    def __preload(self):
        from server.thecore import core

        while 1:
            chunk = self.handle.read(self.record.size)
            if not chunk:
                break
            self.queue.append(self.record.unpack(chunk)[0])

        for uid in self.queue:
            self.objects[uid] = yield core.ioj.get(uid)

        self.__consistent = True

        logging.debug('Queue %s finished preloading', self.name)

        self.__shake()

    @contextmanager
    def __closing(self):
        if self.handle:
            self.handle.flush()
            self.handle.close()
        yield
        self.handle = open(self.filepath, 'a+b')

    def __dump(self):
        if not self.__consistent:
            return
        with self.__closing():
            with open(self.filepath, 'wb') as f:
                for uid in self.queue:
                    f.write(self.record.pack(uid))

