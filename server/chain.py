# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

__author__ = 'viruzzz-kun'


class ChainMastermind:
    chains = {}

    @classmethod
    def register_chain(cls, import_name):
        module = __import__('usr.%s' % import_name, fromlist='*')
        if import_name not in cls.chains:
            cls.chains[import_name] = module
        return module

    @classmethod
    def reload(cls):
        logging.info('Reloading chains: %s...', cls.chains.keys())
        for value in cls.chains.itervalues():
            reload(value)
        logging.info('Chains reloaded: %s', cls.chains.keys())


class ForwardChainProcessor:
    def __init__(self, message):
        self.destinations = set()
        self.message = message

    def process(self):
        module = ChainMastermind.register_chain('chain_forward')
        try:
            module.chain_FORWARD(self.message, self.forward)
        except:
            raise
        finally:
            self.message['destinations'] |= self.destinations

    def forward(self, destination):
        if destination != self.message.get('origin'):
            self.destinations.add(destination)


