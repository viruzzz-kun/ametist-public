#!/usr/bin/env python
# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from libametist.module import TwistedModule

__author__ = "viruzzz-kun"
__date__ = "31.05.2013"
__version__ = "0.1"

class Application(TwistedModule):
    def configure(self, config):
        TwistedModule.configure(self, config)
        self.__module = __import__('usr.loopback_modules.%s' % config['name'], globals(), locals(), ['process'])

    def router_messageReceived(self, message):
        self.sendAck(message.uid)
        for result in (self.__module.process(message) or []):
            self.sendMessage(result)


if __name__ == "__main__":
    Application.main()