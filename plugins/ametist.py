#!/usr/bin/env python
# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from libametist.module import TwistedModule
from twisted.internet import reactor
from twisted.internet.protocol import ClientFactory
from libametist import StreamProtocol

__author__ = "viruzzz-kun"
__date__ = "18.04.2014"
__version__ = "0.1"


class AmetistProtocol(StreamProtocol):
    def __init__(self, app):
        """
        @type app: Application
        """
        StreamProtocol.__init__(self)
        self.app = app
        self.factory = None

    def command_message(self, obj):
        message = obj['message']
        magic = os.urandom(16)
        self.factory.waiting_from_core[magic] = message.uid
        self.app.channel_messageReceived(message, magic)

    def command_msg_ack(self, obj):
        self.app.channel_ackReceived(obj['uid'])


    def command_msg_nack(self, obj):
        self.app.channel_nackReceived(obj['uid'])



class AmetistFactory(ClientFactory):
    protocol = AmetistProtocol

    def __init__(self, app):
        """
        @type app: Application
        """
        self.queue = []
        self.app = app
        self.connection = None
        self.waiting_from_remote = set()
        self.waiting_from_core = {}

    def buildProtocol(self, addr):
        self.connection = protocol = self.protocol(self.app)
        protocol.factory = self
        reactor.callLater(0, self.flush)
        return protocol

    def sendMessage(self, message):
        if self.connection:
            self.sendMessageInt(message)
        else:
            self.queue.append(message)

    def sendMessageInt(self, message):
        obj = {
            'command': 'message',
            'message': message,
        }
        self.connection.sendObject(obj)
        self.waiting_from_remote.add(message.uid)

    def sendAck(self, magic):
        uid = self.waiting_from_core[magic]
        self.connection.sendObject({
            'command': 'msg_ack',
            'uid': uid,
        })

    def clientConnectionLost(self, connector, reason):
        self.app.stop()

    def flush(self):
        for message in self.queue:
            self.sendMessageInt(message)
        self.queue = []


class Application(TwistedModule):
    _factory = None

    def configure(self, config):
        TwistedModule.configure(self, config)
        self._factory = AmetistFactory(self)
        mode = config.get('mode', 'server')
        if mode == 'server':
            reactor.listenTCP(self.config['port'], self._factory, 1, interface=self.config['host'])
        elif mode == 'client':
            reactor.connectTCP(self.config['host'], self.config['port'], self._factory)

    def router_messageReceived(self, message):
        self._factory.sendMessage(message)

    def router_ackReceived(self, magic):
        self._factory.sendAck(magic)

    def channel_messageReceived(self, message, magic=None):
        TwistedModule.channel_messageReceived(self, message, magic)

    def stop(self):
        self._factory.doStop()
        TwistedModule.stop(self)


if __name__ == "__main__":
    Application.main()