#!/usr/bin/python
# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import time

from libametist.module import TwistedModule

__author__ = "viruzzz-kun"
__date__ = "19.05.2013"
__version__ = "0.1"


class Application(TwistedModule):
    def configure(self, config):
        TwistedModule.configure(self, config)

    def router_messageReceived(self, message):
        if 'binary' in message.tags:
            return
        lines = [u'=' * 80, time.strftime('%d.%m.%Y %H:%M')]
        if 'unimas' in message.tags:
            lines.append('%s %s %s' % (message['nahl'], message['timestamp'], message['group'] or ''))
        lines.append(message.payload)
        lines.append(u'=' * 80)
        lines.append(u'\r\n')
        text = u'\r\n'.join(lines).encode(self.config['encoding'])
        with open(self.config['device'], 'wt') as fout:
            fout.write(text)
        self.sendAck(message.oid)


if __name__ == "__main__":
    Application.main()
