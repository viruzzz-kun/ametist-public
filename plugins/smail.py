#!/usr/bin/env python
# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from libametist.message import MessageFormatRegistry

from email.mime.text import MIMEText
from email.header import Header
import poplib
import smtplib
from twisted.internet.task import LoopingCall
import logging

from libametist.module import TwistedModule

__author__ = "viruzzz-kun"
__date__ = "08.05.2013"
__version__ = "0.1"


class Application(TwistedModule):
    lc = None

    def configure(self, config):
        TwistedModule.configure(self, config)
        self.lc = LoopingCall(self.checkMail)
        self.lc.start(config.get('period', 30))

    def router_messageReceived(self, message):
        payload = message.as_string('utf-8', '\n')
        msg = MIMEText(payload, 'plain', 'utf-8')
        to_ = message.get('to', self.config['default-to'])
        from_ = message.get('from', self.config['default-sender'])
        msg['To'] = Header(u'<%s>' % to_)
        msg['From'] = Header(u'<%s>' % from_)
        for name, value in message.iteritems():
            try:
                msg['X-Ametist-Header-' + name] = Header(unicode(value), 'utf-8')
            except:
                pass
        msg['X-Ametist-Tags'] = Header(u'[%s]' % u','.join(tag for tag in message.tags), 'utf-8')
        txt = str(msg)
        with open('/tmp/%s.msg' % message.uid, 'wb') as dump:
            dump.write(txt)
        try:
            logging.info('Connecting to server...')
            smtp = smtplib.SMTP_SSL(self.config['host'], local_hostname='ametist')
            code, resp = smtp.login(self.config['user'], self.config['pass'])
            logging.info('[%s] %s', code, resp)
            smtp.sendmail(from_, to_, txt)
            smtp.quit()
        except Exception, e:
            logging.exception(e.message)
            self.channel_nackReceived(message.uid)
        else:
            logging.info('Message sent')
            self.channel_ackReceived(message.uid)

    def stop(self):
        if self.lc and self.lc.running:
            self.lc.stop()
        TwistedModule.stop(self)

    def checkMail(self):
        try:
            popServer = poplib.POP3(self.config.get('host', ''))
            popServer.user(self.config['user'])
            popServer.pass_(self.config['pass'])
        except Exception, e:
            logging.exception(e.message)
            self.stop()
            return
        numMessages = len(popServer.list()[1])
        for i in xrange(numMessages):
            raw = '\n'.join(popServer.retr(i + 1)[1])
            try:
                message = MessageFormatRegistry.decode('email', raw)
                message.tag(self.config['name'])
                self.channel_messageReceived(message)
            except Exception, e:
                logging.exception(e.message)
                # TODO: Handle
            popServer.dele(i + 1)
        popServer.quit()


if __name__ == "__main__":
    Application.main()