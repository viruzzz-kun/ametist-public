# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import codecs
import logging
import os
from twisted.internet import reactor
from twisted.internet.protocol import Protocol, connectionDone, ClientFactory
from twisted.protocols.basic import _PauseableMixin
from struct import Struct

from libametist import Message
from libametist.message import MessageFormatRegistry
import libametist.my_codecs


codecs.register(libametist.my_codecs.getKoi7)

__author__ = 'viruzzz-kun'


class MTSProtocol(Protocol, _PauseableMixin):
    """MTS/UniMAS TCP protocol implementation"""

    def __init__(self, app):
        """ Construct MTS Protocol
        @param app: application
        @type app: plugins.mts.Application
        """
        self.factory = None
        self.app = app
        self.paused = False
        self._unprocessed = ''
        self.encoding = 'koi8-r'
        self.received = {}

    def dataReceived(self, data):
        """[callback]"""
        all_data = self._unprocessed + data
        currentOffset = 0
        self._unprocessed = all_data
        headerLength = self.headerStruct.size
        while len(all_data) >= (currentOffset + headerLength) and not self.paused:
            messageStart = currentOffset + headerLength
            hdr = all_data[currentOffset:messageStart]
            hdr_data = self.decode_header(hdr)
            if hdr_data['header_type'] == self.MT_ACK:
                message_id = self.factory.msgsInProgress.pop(hdr_data['num'])
                self.app.channel_ackReceived(message_id)
                currentOffset += headerLength
                continue
            messageEnd = messageStart + hdr_data['len']
            fullEnd = messageEnd + headerLength
            if len(all_data) < fullEnd:
                break
            packet = all_data[messageStart:messageEnd]
            message = self.decode_message(hdr_data, packet)
            currentOffset = fullEnd
            magic = os.urandom(16)
            self.app.channel_messageReceived(message, magic)
            self.received[magic] = hdr_data
        self._unprocessed = all_data[currentOffset:]

    def sendMessage(self, msg):
        """Sends message to the channel

        @param msg: a api.Message to send
        @type msg: Message"""
        msg['num'] = msg['ijp'] = self.factory.num
        if isinstance(msg.payload, Message):
            msg.payload['num'] = self.factory.num
        packet = self.encode_message(msg)
        self.factory.msgsInProgress[msg['num']] = msg.uid
        self.transport.write(packet)

    def connectionMade(self):
        self.factory.flush_queued_messages(self)

    def sendAck(self, magic):
        header = self.received.pop(magic, None)
        if not header:
            return
        header['header_type'] = self.MT_ACK
        self.transport.write(self.encode_header(header))

    def connectionLost(self, reason=connectionDone):
        logging.warn('Connection lost because %s', reason.getErrorMessage())
        self.factory.current_protocol = None
        self.app.stop()

    # data manipulation

    MT_DATA = 1
    MT_Z = 5
    MT_ACK = 2
    MT_END = 4
    MT_RR = 6
    MT_COMPRESS = 128
    headerStruct = Struct("!BxxxLHH10sxxBxxx")

    # The Miraculous 28-byte Header

    @classmethod
    def decode_header(cls, raw):
        """Makes header object of raw 28-byte data

        @param raw: raw 28-byte data
        @type raw: str
        @return: header dict
        """
        header = dict()
        (header['header_type'], header['ijp'], header['len'], header['num'], header['ahl'], header['priority']) = cls.headerStruct.unpack(raw)
        header['ahl'] = header['ahl'].decode('koi7-n2')
        return header

    @classmethod
    def encode_header(cls, header):
        return cls.headerStruct.pack(header['header_type'], header['ijp'], header['len'], header['num'], header['ahl'].encode('koi7-n2'), header['priority'])

    # The Magical Message Body

    def decode_message(self, header, body):
        """ Make Message from header data and binary body
        @type header: dict
        @type body: str
        """
        try:
            body = MessageFormatRegistry.decode('soh', body, None).payload
        except ValueError:
            pass
        try:
            body = MessageFormatRegistry.decode('zczc', body, None).payload
        except ValueError:
            pass
        try:
            message = MessageFormatRegistry.decode('wmo', body, self.encoding)
        except ValueError:
            try:
                message = MessageFormatRegistry.decode('hms', body, self.encoding)
            except ValueError or UnicodeDecodeError:
                message = Message()
                message.tag('raw')
                message.payload = body
        message.update({'ahl': header['ahl'], 'ijp': header['ijp'], 'priority': header['priority'], })
        return message

    def encode_message(self, message):
        """ Make binary 3-packet form Message
        @type message: Message
        """
        if not message.format in ('wmo', 'hms'):
            logging.critical('Can only encode WMO or HMS message payload, got %s instead (uid=%s)', type(message), message.uid)
            raise RuntimeError('Can only encode WMO or HMS message payload, got %s instead', type(message))
        ijp = message.get('ijp', message['num'])
        ahl = message['ahl']
        priority = message.get('priority', 1)
        body = MessageFormatRegistry.encode('soh', message, self.encoding, delimiter='\r\r\n')
        header = self.headerStruct.pack(self.MT_DATA, ijp, len(body), ijp, ahl.encode('koi7-n2'), priority)
        ass = self.headerStruct.pack(self.MT_END, ijp, len(body), ijp, ahl.encode('koi7-n2'), priority)
        return b'%s%s%s' % (header, body, ass)


class MTSFactory(ClientFactory):
    def __init__(self, app, koi7=False):
        self.app = app
        self.koi7 = koi7
        self.current_protocol = None
        self.msgsInProgress = {}
        self.msgsQueued = []
        self.num = 0

    def buildProtocol(self, addr):
        protocol = MTSProtocol(self.app)
        if self.koi7:
            protocol.encoding = 'koi7-n2'
        protocol.factory = self
        self.app._mts_protocol = protocol
        if self.current_protocol:
            self.current_protocol.transport.loseConnection()
        self.current_protocol = protocol
        self.app.sendExtState('connected')
        return protocol

    def sendMessage(self, message):
        if not message.format in ('wmo', 'hms'):
            logging.critical(u'Cannot send message uid=%d because of type=%s', message.uid, type(message))
            # But still say, it went okay, because we don't want to stop.
            reactor.callLater(0, self.app.channel_ackReceived, message.uid)
            return
        self.msgsQueued.append(message)
        self.flush_queued_messages(self.current_protocol)

    def clientConnectionFailed(self, connector, reason):
        reactor.callLater(10, connector.connect)

    def flush_queued_messages(self, protocol):
        """
        @type protocol: MTSProtocol
        """
        if protocol:
            while self.msgsQueued:
                msg = self.msgsQueued.pop(0)
                try:
                    self.sendMessageInt(msg, protocol)
                except IOError:
                    self.msgsQueued.append(msg)
                    break

    def sendMessageInt(self, message, protocol):
        """
        @type message: Message
        @type protocol: MTSProtocol
        """
        protocol.sendMessage(message)
        self._increment_mts_num()

    def _increment_mts_num(self):
        self.num += 1
        if self.num >= 1000:
            self.num = 0
