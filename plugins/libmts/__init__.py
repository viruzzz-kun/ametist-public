#-*- coding: utf-8 -*-
from datetime import datetime

__author__ = "viruzzz-kun"
__date__ = "20.04.2013"


def get_wmo_timestamp():
    return datetime.utcnow().strftime("%d%H%M")


def get_full_stamp():
    return datetime.utcnow().isoformat()


class CustomException(Exception):
    pass




