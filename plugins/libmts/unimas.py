# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import fnmatch
import re

__author__ = 'viruzzz-kun'


class UniMASUtils:
    hms_koi = [u'ЮАБЦДЕФГХИЙКЛМНОПЯРСТУЖВЬЫЗШЭЩЧЪ',
               u'@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_']
    re_rou_line = re.compile(
        ur"\s*(?P<match>[^\s#]+)\s*:"
        ur"\s*(?P<duplicate>[+-])\s*:"
        ur"\s*(?P<priority>[1234])\s*:"
        ur"\s*(?P<conv>\w*)\s*:"
        ur"\s*(?P<stop>[yn])\s*:"
        ur"\s*(?P<dest>.*)\s*"
        ur"(#.*)?",
        re.IGNORECASE)

    def __init__(self):
        self.rou = {
            'HMS': [],
            'WMO': [],
        }

    def hms2std(self, string):
        string = string.upper()
        return ''.join(
            (self.hms_koi[1][self.hms_koi[0].index(char)]
             if char in self.hms_koi[0] else char)
            for char in string
        )

    def loadROU(self, names_dict):
        """Load UniMAS Routing table

        @param names_dict: dict of message type -> filename"""
        self.rou = {
            'HMS': [],
            'WMO': [],
        }
        for name, filename in names_dict.iteritems():
            with open(filename) as rou:
                for line in rou:
                    wline = None
                    for enc in ('koi8-r', 'utf-8'):
                        try:
                            wline = line.decode(enc)
                        except UnicodeError:
                            pass
                    if not wline:
                        continue
                    try:
                        group = UniMASUtils.re_rou_line.match(wline).groupdict()
                    except AttributeError:
                        continue
                    else:
                        gmatch = self.hms2std(group['match'])
                        match = re.compile(fnmatch.translate(gmatch), re.IGNORECASE)
                        duplicate = group['duplicate'] == '+'
                        priority = int(group['priority'])
                        stop = group['stop'].lower() == 'y'
                        dest = group['dest']
                        destinations = set(map(unicode.strip, dest.split(','))) if dest else set()
                        self.rou[name].append((match, duplicate, priority, stop, destinations))

    def tag(self, message):
        """Expand message tags to conform UniMAS routing table

        @param message: a message :)"""
        if 'hms' in message.tags:
            chain = list(self.rou['HMS'])
        else:
            chain = list(self.rou['WMO'])
        dup_check = False
        if not 'raw' in message.tags:
            for match, duplicate, prio, stop, dest in chain:
                if match.match(self.hms2std(message['ahl'])):
                    message['priority'] = prio
                    message |= dest
                    dup_check |= duplicate
                    if stop:
                        break
