#!/usr/bin/env python
# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from libametist.message import MessageFormatRegistry
import logging
from email.mime.text import MIMEText
from email.header import Header
from twisted.cred import credentials, error
from twisted.python import failure
from twisted.internet import defer, reactor
from twisted.mail import smtp
from twisted.mail.imap4 import LOGINCredentials, PLAINCredentials
from twisted.cred.checkers import ICredentialsChecker
from twisted.cred.portal import IRealm, Portal
from twisted.internet.ssl import ClientContextFactory

from libametist.module import TwistedModule

from zope.interface import implements

__author__ = "viruzzz-kun"
__date__ = "18.04.2014"
__version__ = "0.1"


class AmetistMessageDelivery:
    implements(smtp.IMessageDelivery)

    class BlackHole:
        implements(smtp.IMessage)

        def lineReceived(self, line):
            pass

        def eomReceived(self):
            return defer.succeed(None)

        def connectionLost(self):
            pass

    class Message:
        implements(smtp.IMessage)

        def __init__(self, app):
            """
            @type app: Application
            """
            self.app = app
            self.lines = []

        def lineReceived(self, line):
            self.lines.append(line)

        def eomReceived(self):
            raw = "\n".join(self.lines)
            try:
                # with open('/tmp/%.3f.msg' % time.time(), 'wb') as f:
                #     f.write(raw)
                message = MessageFormatRegistry.decode('email', raw)
                self.app.channel_messageReceived(message)
            except Exception, e:
                logging.exception(e.message)

            self.lines = None
            return defer.succeed(None)

        def connectionLost(self):
            # There was an error, throw away the stored lines
            self.lines = None

    def __init__(self, app):
        """
        @type app: Application
        """
        self.app = app

    def receivedHeader(self, helo, origin, recipients):
        """
        @type helo: (str, str)
        @type origin: twisted.mail.smtp.Address
        @type recipients: list(User)
        """
        logging.info(
            u'Received message from %s (%s) to %s',
            unicode(origin), helo[1], [unicode(rec) for rec in recipients]
        )
        return "Received: AmetistMessageDelivery"

    def validateFrom(self, helo, origin):
        """
        @type helo: (str, str)
        @type origin: twisted.mail.smtp.Address
        """
        # All addresses are accepted
        return origin

    def validateTo(self, user):
        if unicode(user.dest) == self.app.config['username']:
            return self.create_message
        logging.warning('Message to %s is sent to BlackHole', unicode(user))
        return self.create_blackhole
        # raise smtp.SMTPBadRcpt(user)

    def create_message(self):
        return self.Message(self.app)

    def create_blackhole(self):
        return self.BlackHole()


class AmetistSMTPFactory(smtp.SMTPFactory):
    protocol = smtp.ESMTP

    def __init__(self, portal, app):
        smtp.SMTPFactory.__init__(self, portal)
        self.context_factory = ClientContextFactory()
        self.delivery = AmetistMessageDelivery(app)

    def buildProtocol(self, addr):
        p = smtp.SMTPFactory.buildProtocol(self, addr)
        p.delivery = self.delivery
        p.ctx = self.context_factory
        p.challengers = {"LOGIN": LOGINCredentials, "PLAIN": PLAINCredentials}
        return p


class Application(TwistedModule):
    implements(IRealm, ICredentialsChecker)

    credentialInterfaces = (credentials.IUsernamePassword, credentials.IUsernameHashedPassword)

    def configure(self, config):
        TwistedModule.configure(self, config)
        portal = Portal(self, (self,))
        reactor.listenTCP(
            int(config['receive']['port']),
            AmetistSMTPFactory(portal, self),
            interface=config['receive']['host']
        )

    def router_messageReceived(self, message):
        import smtplib
        payload = message.as_string('utf-8', '\n')
        msg = MIMEText(payload, 'plain', 'utf-8')
        to_ = message.get('to', self.config['default-to'])
        from_ = message.get('from', self.config['default-sender'])
        msg['To'] = Header(u'<%s>' % to_)
        msg['From'] = Header(u'<%s>' % from_)
        for name, value in message.iteritems():
            try:
                msg['X-Ametist-Header-' + name] = Header(unicode(value), 'utf-8')
            except:
                pass
        msg['X-Ametist-Tags'] = Header(u'[%s]' % u','.join(tag for tag in message.tags), 'utf-8')
        txt = str(msg)
        with open('/tmp/%s.msg' % message.uid, 'wb') as dump:
            dump.write(txt)
        try:
            logging.info('Connecting to server...')
            if self.config['send'].get('method', 'SSL') == 'SSL':
                smtp = smtplib.SMTP_SSL(
                    self.config['send']['host'],
                    self.config['send'].get('port', 25),
                    local_hostname='ametist'
                )
            else:
                smtp = smtplib.SMTP(
                    self.config['send']['host'],
                    self.config['send'].get('port', 25),
                    local_hostname='ametist'
                )
            if self.config['self'].get('method', 'SSL') == 'STARTTLS':
                smtp.starttls()
            code, resp = smtp.login(self.config['user'], self.config['pass'])
            logging.debug('[%s] %s', code, resp)
            smtp.sendmail(from_, to_, txt)
            smtp.quit()
        except Exception, e:
            logging.exception(e.message)
            self.channel_nackReceived(message.uid)
        else:
            logging.info('Message sent')
            self.channel_ackReceived(message.uid)

    # ICredentialsChecker

    def _cbPasswordMatch(self, matched, username):
        return username if matched else failure.Failure(error.UnauthorizedLogin())

    def requestAvatarId(self, credentials):
        if credentials.username in self.config['users']:
            logging.debug('Login attempt from %s succeeded', credentials.username)
            return defer.maybeDeferred(credentials.checkPassword, self.config['users'][credentials.username]).addCallback(
                self._cbPasswordMatch, str(credentials.username))
        else:
            logging.debug('Login attempt from %s failed', credentials.username)
            return defer.fail(error.UnauthorizedLogin())

    # IRealm

    def requestAvatar(self, avatarId, mind, *interfaces):
        if smtp.IMessageDelivery in interfaces:
            return smtp.IMessageDelivery, AmetistMessageDelivery(self), lambda: None
        raise NotImplementedError()


if __name__ == "__main__":
    Application.main()