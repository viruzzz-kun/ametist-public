#!/usr/bin/env python
# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from libametist import serialize

import os

from libametist.module import TwistedModule
from libametist.utils import ensure_dir_exists

__author__ = "viruzzz-kun"
__date__ = "28.04.2013"
__version__ = "0.1"


class Application(TwistedModule):
    def configure(self, config):
        TwistedModule.configure(self, config)
        dirname = os.path.dirname(config['filename'])
        ensure_dir_exists(dirname)

    def router_messageReceived(self, message):
        with open(self.config['filename'], 'at') as f:
            f.write(serialize.dump(message))
        self.sendAck(message.uid)


if __name__ == "__main__":
    Application.main()