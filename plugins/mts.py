#!/usr/bin/env python
# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from twisted.internet import reactor

from libametist.module import TwistedModule
from libmts.protocol import MTSFactory
from libmts.unimas import UniMASUtils

__author__ = "viruzzz-kun"
__date__ = "28.04.2013"
__version__ = "0.1"


class Application(TwistedModule):
    def configure(self, config):
        TwistedModule.configure(self, config)
        self._rou = UniMASUtils()
        if 'ROU' in config:
            self._rou.loadROU(config['ROU'])
        koi7 = bool(config.get('koi-7', False))
        self._mts_factory = MTSFactory(self, koi7)
        if self.config['mode'] == 'server':
            reactor.listenTCP(self.config['port'], self._mts_factory, interface=self.config['host'])
        elif self.config['mode'] == 'client':
            reactor.connectTCP(self.config['host'], self.config['port'], self._mts_factory)

    def router_messageReceived(self, message):
        self._mts_factory.sendMessage(message)

    def router_ackReceived(self, magic):
        self._mts_protocol.sendAck(magic)

    def channel_messageReceived(self, message, magic=None):
        self._rou.tag(message)
        TwistedModule.channel_messageReceived(self, message, magic)

    def stop(self):
        self._mts_factory.doStop()
        TwistedModule.stop(self)


if __name__ == "__main__":
    Application.main()