#!/usr/bin/env python
# -*- coding: utf-8 -*-

# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from libametist.module import TwistedModule

__author__ = "viruzzz-kun"
__date__ = "28.04.2013"
__version__ = "0.1"


class Application(TwistedModule):
    def router_messageReceived(self, message):
        with open('./.tmp/%s.dmp' % (message['filename']), 'wb') as fOut:
            fOut.write(message.payload)
        self.sendAck(message.oid)



if __name__ == "__main__":
    Application.main()