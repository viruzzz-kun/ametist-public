# -*- coding: utf-8 -*-
#
# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, render_template
import os
from blueprints.control import module as core_control
from blueprints.send import module as send
from blueprints.dpms import module as dpms

__author__ = 'mmalkov'

www_root = os.path.join(os.path.dirname(__file__))
template_folder = os.path.join(www_root, 'templates')
static_folder = os.path.join(www_root, 'static')

app = Flask(__name__, template_folder=template_folder, static_folder=static_folder)

app.register_blueprint(core_control, url_prefix='/core')
app.register_blueprint(send, url_prefix='/send')
app.register_blueprint(dpms, url_prefix='/dpms')

@app.route('/')
def index():
    return render_template('index.html')

@app.context_processor
def config():
    from server.thecore import core
    return {
        'app_config': core.get_config('flask-app')
    }