/**
 * Created by mmalkov on 10.02.14.
 */
var Ametist = angular.module('Ametist', ['ui.bootstrap', 'ngSanitize', 'simple.WebSocket']);
Ametist.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

var aux = {
    getQueryParams: function (qs) {
        qs = qs.split("+").join(" ");

        var params = {}, tokens,
                re = /[?&]?([^=]+)=([^&]*)/g;

        while (tokens = re.exec(qs)) {
            params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
        }

        return params;
    },
    range: function (num) {
        return Array.apply(null, new Array(num)).map(function(_, i) {return i;})
    },
    moment: moment,
    endswith: function (str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    },
    startswith: function (str, prefix) {
        return str.indexOf(prefix) === 0;
    },
    removeitem: function (array, item) {
        var index = array.indexOf(item);
        if (index === -1) return;
        array.splice(index, 1);
    },
    allin: function (what, in_what) {
        return what.filter(function (i) {
            return in_what.indexOf(i) !== -1
        }).length == what.length;
    }
};