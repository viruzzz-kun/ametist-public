/**
 * Created by viruzzz-kun on 29.05.2014.
 * (c) 2014 Mikhael Malkov
 * License: MIT
 */
angular.module('simple.WebSocket', [])
.factory('$WebSocket', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
    var Socket = function (address) {
        this.address = address;
        this._reconnect = null;
        this.ws = null;
        this.timeout = 5000;
        this.expect_disconnect = false;
        this.queue = [];
    };
    Socket.prototype.connect = function () {
        this.expect_disconnect = false;
        var self = this;
        var ws = new WebSocket(this.address);
        ws.onopen = function (event) {
            if (self._reconnect) {
                $timeout.cancel(self._reconnect);
                self._reconnect = null;
            }
            self.ws = ws;
            if (self._on_open_callback) {
                self._on_open_callback();
            }
            var i;
            for (i=0; i < self.queue.length; i++) {
                self.ws.send(JSON.stringify(self.queue[i]));
            }
            self.queue = [];
        };
        ws.onclose = function (event) {
            if (self._on_disconnect_callback) {
                $rootScope.$apply(self._on_disconnect_callback)
            }
            if (!self.expect_disconnect) {
                self._reconnect = $timeout(function () {
                    self.connect.apply(self);
                }, self.timeout);
                self.ws = null;
            }
        };
        ws.onerror = function (event) {
            ws.close();
        };
        ws.onmessage = function (event) {
            if (self._on_message_callback) {
                $rootScope.$apply(function () {
                    self._on_message_callback(JSON.parse(event.data))
                })
            }
        };
    };
    Socket.prototype.send = function (object) {
        if (this.ws) {
            this.ws.send(JSON.stringify(object));
        } else {
            this.queue.push(object);
        }
    };
    Socket.prototype.disconnect = function () {
        this.expect_disconnect = true;
        if (this._reconnect) {
            clearInterval(this._reconnect);
        } else {
            this.ws.close();
        }
    };
    Socket.prototype.onMessage = function (f) {
        this._on_message_callback = f;
    };
    Socket.prototype.onOpen = function (f) {
        this._on_open_callback = f;
    };
    Socket.prototype.onDisconnect = function (f) {
        this._on_disconnect_callback = f;
    };
    return Socket;
}]);