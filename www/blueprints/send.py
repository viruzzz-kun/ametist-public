# -*- coding: utf-8 -*-
#
# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import crochet
from flask import Blueprint, render_template, request, abort
import shutil
import yaml
from libametist import Message

from server.thecore import core
from ..utils import std_response


__author__ = 'viruzzz-kun'


module = Blueprint('Send', __name__)


@module.route('/index.html')
def html_index():
    return render_template(
        'send.index.html'
    )


@module.route('/api/templates.json')
def api_templates():
    with open('./etc/ametist/message_templates.yaml', 'rt') as f:
        templates = yaml.load(f)
    return std_response([{
        'name': name,
        'data': data,
    } for name, data in templates.iteritems()])


@crochet.run_in_reactor
def gate_send_message(message):
    core.dispatch_message(message, 'xsend')


@module.route('/api/send-wmo-message', methods=['POST'])
def api_send_wmo_message():
    inp = request.json
    if not inp:
        return abort(400)
    addr = inp.get('hms_addr')
    message = Message()
    if addr:
        message['hms_addr'] = addr
        message.format = 'hms'
    else:
        message.format = 'wmo'
    message.payload = inp.get('text')
    message['ahl'] = inp.get('ahl')
    message['timestamp'] = inp.get('timestamp')
    group = inp.get('group')
    if group:
        message['group'] = group
    message.tag('wmo', 'xsend')
    gate_send_message(message)
    return ''


@module.route('/api/save-wmo-template', methods=['POST'])
def api_save_wmo_template():
    inp = request.json
    with open('./etc/ametist/message_templates.yaml', 'rt') as f:
        templates = yaml.safe_load(f)
    templates[inp['name']] = inp['data']
    with open('./etc/ametist/message_templates.yaml.tmp', 'wt') as f:
        yaml.safe_dump(templates, f, indent=2, default_flow_style=False, encoding='utf-8')
    shutil.move('./etc/ametist/message_templates.yaml.tmp', './etc/ametist/message_templates.yaml')
    return std_response([{'name': name, 'data': data, } for name, data in templates.iteritems()])
