# !/usr/bin/python
# -*- coding: utf-8 -*-
#
# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import crochet

import xml.etree.ElementTree as ET

from flask import Blueprint, request, render_template, abort
from datetime import datetime
import dateutil

__author__ = "viruzzz-kun"
__date__ = "01.06.2014"
__version__ = "0.1"

module = Blueprint('E2EDM-SP', __name__)


def render_xml_response(template_name_or_list, **context):
    return \
        render_template('e2edm-sp/xml.xml', body=render_template(template_name_or_list, **context)), \
        200, \
        [('content-type', 'text/xml; charset=UTF-8')]


def wrap_xml_response(string):
    return \
        render_template('e2edm-sp/xml.xml', body=string), \
        200, \
        [('content-type', 'text/xml; charset=UTF-8')]


def wrap_plain_response(string):
    return string, 200, [('content-type', 'text/plain; charset=UTF-8')]


@module.route('/controller', methods=['GET'])
def controller_get():
    action = request.args.get('action')
    if action == "getResourceCache":
        return getResourceCache()

    elif action == "getResource":
        return getResource(request.args['resource'])

    elif action == "ping":
        return wrap_plain_response('ping? pong!')

    elif action == "getVersion":
        return wrap_plain_response('1.8.666 Satanic Version')

    elif action == "getUpdateDate":
        return wrap_plain_response(__date__)

    else:
        return "418 I'm a teapot", 418


@module.route('/controller', methods=['POST'])
def controller_post():
    doc = request.values.get('doc', None)
    if doc is None:
        return abort(400)
    return wrap_xml_response(dispatchXMLRequest(doc))


@crochet.wait_for(timeout=1)
def get_config():
    from server.thecore import core

    return core.get_config('flask-app')['e2edm-sp']


def getResourceDict(name):
    """Get dictionary describing resource
    @param name: resource name
    @return resource description
    @rtype dict"""
    config = get_config()
    resource = dict(config['resources-default'])
    resource.update(config['resources'][name])
    resource['name'] = name
    return resource


def getTransportFileURL(resourceName, sendTime):
    """Get resource transport file URL.
    @status: unfinished
    @param resourceName: name of the resource
    @param sendTime: time of resource actuality
    @return: transport file URL
    @rtype: unicode"""
    config = get_config()
    localFilePath = config['local_nc_path']
    remoteFilePath = config['remote_nc_path']
    fileName = resourceName + '.nc'
    return u"%(remoteNcPath)s/%(filename)s" % {
        'remoteNcPath': remoteFilePath,
        'filename': fileName
    }


def getResource(name):
    """Get resource description in XML form. (for /dpms/controller?action=getResource)
    @param name: resource Name
    @return: XML resource description
    @rtype: unicode"""
    try:
        resource = getResourceDict(name)
        return render_xml_response('e2edm-sp/resource.xml', resource=resource)
    except KeyError:
        return wrap_plain_response('Resource not found')


def getResourceCache():
    """Get resources description in XML form. (for /dpms/controller?action=getResourceCache)
    @return: XML resources description
    @rtype: unicode"""
    config = get_config()
    return wrap_xml_response("\n".join([
        render_template(
            'e2edm-sp/resource.xml',
            resource=getResourceDict(name)
        )
        for name in sorted(config['resources'].iterkeys())
    ]))


def dispatchXMLRequest(xmlDocument):
    """Method to dispatch XML document in HTTP POST request (/dpms/controller)
    @param xmlDocument: incoming XML document as string
    @return: XML answer to request
    @rtype: unicode"""
    config = get_config()
    doc = ET.fromstring(xmlDocument)
    ns = config['xml_namespaces']

    sendTime = doc.find('n1:header/n1:sendTime', namespaces=ns)
    sendTime = dateutil.parse(sendTime).replace(tzinfo=None) if sendTime else datetime.datetime.utcnow()
    resourceName = doc.find('n1:header/n1:destination', namespaces=ns).attrib['resource']
    requestType = doc.find('n1:header/n1:type', namespaces=ns).text.strip()

    return render_xml_response(
        'e2edm-sp/response_search.xml',
        resource=getResourceDict(resourceName),
        # Transport File must be generated for this request
        transportFileURL=getTransportFileURL(resourceName, sendTime),
        requestType=requestType,
        sendTime=datetime.datetime.utcnow().isoformat()[:19]
    )
