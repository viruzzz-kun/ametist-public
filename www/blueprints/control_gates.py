#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import crochet
from twisted.internet.defer import Deferred
from server.thecore import core

__author__ = 'viruzzz-kun'
__created__ = '02.05.14'


@crochet.run_in_reactor
def gate_core_control(command):
    if command == 'start':
        core.start()
    elif command == 'stop':
        core.stop()
    elif command == 'restart':
        core.stop()
        core.start()
    elif command == 'reload':
        from server.chain import ChainMastermind
        ChainMastermind.reload()


@crochet.run_in_reactor
def gate_core_channel(channel, command):
    if command == 'start':
        core.start_channel(channel)
    elif command == 'stop':
        core.stop_channel(channel)
    elif command == 'restart':
        core.restart_channel(channel)
    elif command == 'purge':
        core.purge_channel(channel)


@crochet.run_in_reactor
def gate_core_inject(config, callback):
    core.ioj.filter(config, callback)


@crochet.wait_for(timeout=10)
def gate_ioj_minimum():
    return core.ioj.get_min_timestamp()


@crochet.wait_for(timeout=15)
def gate_ioj_hour(config, callback=None, finish=None):
    result = []
    d = Deferred()

    def _cbMsg(message):
        result.append({
            'uid': message.uid,
            'ahl': message.get('ahl'),
            'o': message.get('origin'),
            'd': list(message.get('destinations')),
            't': list(message.tags),
        })

    def _cbFinish():
        d.callback(result)

    core.ioj.filter(config, callback if callable(callback) else _cbMsg, finish if callable(finish) else _cbFinish)

    return d


@crochet.wait_for(timeout=10)
def gate_ioj_uid(uid):
    return core.ioj.get(uid)