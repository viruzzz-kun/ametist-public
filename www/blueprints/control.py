# -*- coding: utf-8 -*-
#
# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import calendar
from crochet import TimeoutError
import datetime
import logging
import time
import traceback
from traceback import format_exc
from twisted.internet.defer import CancelledError
from flask import Blueprint, abort, render_template, request,redirect, url_for

from .control_gates import gate_core_control, gate_core_channel, gate_core_inject, gate_ioj_minimum, gate_ioj_hour, gate_ioj_uid
from server.channel import Phase
from www.utils import std_response
from server.thecore import core


__author__ = 'viruzzz-kun'

module = Blueprint('CoreControl', __name__)


@module.context_processor
def core_channel_names():
    return {
        'channels': core.channels.keys(),
        'get_stat': core.get_stat,
        'Phase': Phase,
    }

@module.route('/index.html')
def html_index_redirect():
    return redirect(url_for('.html_index'))


@module.route('/')
def html_index():
    return render_template('core.index.html')


@module.route('/api/core/')
@module.route('/api/core/<command>', methods=['POST'])
def api_core_control(command=None):
    try:
        gate_core_control(command)
    except:
        return std_response(traceback.format_exc())
    return std_response(None)


@module.route('/api/channel/')
@module.route('/api/channel/<channel>/<command>', methods=['POST'])
def api_channel_control(channel, command):
    if channel not in core.channels:
        return abort(404)
    try:
        gate_core_channel(channel, command)
    except:
        return std_response(traceback.format_exc())
    return std_response(None)


@module.route('/api/inject', methods=['POST'])
def api_inject():
    now = time.time()
    logging.debug('%s', request.json)
    j = request.json
    config = {
        'conditions': {
            'beg': now - 3600,
            'end': now,
        },
        'actions': {},
    }
    inputs = j.get('inputs')
    if inputs:
        config['conditions']['origin'] = inputs.split()
    
    ahl = j.get('ahl')
    if ahl:
        config['conditions']['ahl'] = ahl
    
    tags_p = j.get('tags_p')
    tags_m = j.get('tags_m')
    if tags_p or tags_m:
        tp = tags_p.split() if tags_p else []
        tm = tags_m.split() if tags_m else []
        config['conditions']['tags'] = [tp, tm]

    dests_p = j.get('dests_p')
    dests_m = j.get('dests_m')
    if dests_p or dests_m:
        dp = dests_p.split() if dests_p else []
        dm = dests_m.split() if dests_m else []
        config['conditions']['dests'] = [dp, dm]

    destinations = j.get('dests')
    if destinations:
        dests = [name for (name, val) in destinations.items() if val]
        if dests:
            config['actions']['destinations'] = dests

    logging.debug('config = %s', config)

    gate_core_inject(config, core.inject_message)
    return std_response('Started...')


@module.route('/api/ioj/minimum.json')
def api_ioj_minimum():
    return std_response(gate_ioj_minimum())


@module.route('/api/ioj/date/')
@module.route('/api/ioj/date/<the_date>')
def api_ioj_thedate(the_date):
    try:
        d = datetime.datetime.strptime(the_date, '%Y%m%d%H%M')
    except ValueError:
        return abort(402)

    timestamp = calendar.timegm(d.utctimetuple())
    config = {
        'conditions': {
            'beg': timestamp,
            'end': timestamp + 900,
        },
        'actions': {},
    }
    try:
        result = gate_ioj_hour(config)
    except CancelledError or TimeoutError:
        return abort(504)

    ret = std_response(result)
    logging.debug('Finished making response')
    return ret


@module.route('/api/ioj/uid/')
@module.route('/api/ioj/uid/<int:uid>')
def api_ioj_uid(uid):
    try:
        message = gate_ioj_uid(uid)
    except KeyError:
        return abort(404)
    except:
        return format_exc(), 404
    else:
        if message.has_tags('text') or message.no_tags('binary', 'BUFR', 'FAX', 'GRIB'):
            return message.as_string(delimiter='<br/>\n'), 200, [('content-type', 'text/html; encoding=utf-8')]
        elif message.has_tags('binary'):
            message = message.clone()
            message.payload = 'BINARY'
        return message.as_string(delimiter='<br/>\n'), 200, [('content-type', 'text/html; encoding=utf-8')]

@module.route('/api/ioj/uid/<int:uid>/raw')
def api_uij_uid_raw(uid):
    try:
        message = gate_ioj_uid(uid)
    except KeyError:
        return abort(404)
    except:
        return format_exc(), 404
    else:
        return message.as_string(delimiter='\n'), 200, [('content-type', 'application/octet-stream')]