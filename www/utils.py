# -*- coding: utf-8 -*-
#
# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import json

__author__ = 'viruzzz-kun'


def std_response(obj, ret_code=200, **kwargs):
    result = {
        'meta': {
            'ret_code': ret_code
        },
        'result': obj
    }
    result.update(kwargs)
    return (
        json.dumps(result, encoding='utf-8', ensure_ascii=False),
        200,
        [('content-type', 'application/json; encoding=utf-8')]
    )