AMETIST Configuration
=====================

Basic configuration
-------------------

`etc/ametist.yaml` is the main configuration file. It has several required root nodes:

**ioj** defines configuration for IO journal - database where all incoming messages from all the channels are stored
  
  - **path** is the root path of the database
  - **host** is the IP address IOJ will listen on
  - **port** is the TCP port IOJ will listen on

**queue-pool** defines configuration for persistent queues used to pump data to channel processes
  
  - **root** is the filesystem root path for queues

**flask-app** defines configuration for web-interface, either ReST or HTML
  
  - **interface** is the IP address webface will listen on
  - **port** is the TCP port webface will listen on
  - **ws-path** is the part of URL on which the WebSocket is open
  
  Also, the plugin E2EDM-SP (E2EDM Simple Provider) is configured here
  
  - **e2edm-sp**

**channels** defines configuration for channel processes. Keys of this mapping are the unique system-wide names of 
channels and values are configurations itselves. When channel process starts it receives exactly these values _plus_ 
the unique name. Configuration keys and values are dictated by the channel type, its implementation, however key 
**module** must always be present in configuration as it tells Core the executable name (under ./plugins directory)

  - **mts.py** implements MTS/TRANSMET/UniMAS TCP protocol (compression and RR-packets are not implemented)
    - **mode** - 'client' or 'server'
    - **host** in client mode - the host name or IP address to connect to, in server - interface address to listen to
    - **port** in client mode - port number to connect to, in server - port to listen to
    - **koi-7** (true or not present) - when set to true will treat HMS messages being KOI-7 N2 encoded
    - **ROU**
  - **fileout.py** dumps messages to file in YAML format
    - **filename** - file name
  - **smtp_server.py** implements SMTP protocol to either receive or send messages in WMO and HMS formats (SOH and ZCZC transport 
  wraps supported)
    - **username** - default value for *From* header in outgoing mail
    - **receive** - defines incoming configuration
      - **host**
      - **port**
      - **protocol** - SMTP or ESMTP
    - **send** defines outgoing configuration
      - **host**
      - **port**
      - **method** - SMTP or STARTTLS or SSL
      - **user** - username on the remote server
      - **pass** - password
    - **users** - mapping of usernames -> passwords which are allowed to send messages to Ametist
  - **smail.py** implements simple mail - like **smtp_server.py** it sends message to remote SMTP-server, however 
  receives by polling POP3-server.
    - **host**
    - **user** - username on the remote POP3-server
    - **pass** - password
    - **period** - period to poll server in seconds
    - **default-sender**
    - **default-to**
  - **loopback.py** receives messages from Core, makes some operations on them and send them back. It has no means to 
  communicate with outer world. Processing rules must be defined in module `usr.loopback_modules.`{{`name`}} where 
  {{`name`}} is the unique channel name. Module must have function `process` with the only argument - message 
  (`def process(message)`) and return list of processed messages. For example, no-change loopback:
  >>> def process(message):
  >>>      return [message]
  It is useful when receiving a multipart email with 
  mixed data (for example, few SYNOP messages and a couple of maps) which must be decomposed and sent to different
  destinations; or unfolding Envelope messages

Routing
-------

`usr.chain_forward` is the module which defines rules for routing messages. It has only function 
`def chain_FORWARD(message, to)` which gets message as the first parameter and function `to` as the second. Function 
`to` is used to add destination for message. 

For example, if we want to route all HMS messages to channel **smtp** we write like this:
>>> def chain_FORWARD(message, to):
>>>     if message.format == 'hms':
>>>         to('smtp')
Virtually any message parameter can be used for routing, including text body, headers, format and, most important, tags.
