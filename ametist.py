#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# AMETIST - AMETIST Meteorological Information System
# Copyright (C) 2014  Mikhael Malkov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import sys
import crochet
from twisted.internet import reactor

logging.basicConfig(
    format='%(asctime)s: %(process)5d [%(levelname)s] %(filename)s:%(lineno)d %(message)s',
    stream=sys.stderr,
    level=logging.DEBUG
)

crochet.no_setup()
# noinspection PyUnresolvedReferences
import libametist.formats

__author__ = "viruzzz-kun"
__date__ = "28.04.13"

if __name__ == '__main__':
    from server.thecore import core

    reactor.callLater(0, core.initialize)
    reactor.callLater(0, core.start)

    reactor.run()